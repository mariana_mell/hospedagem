from django.db import models
from . import views


class Login(models.Model):
    nome = models.CharField(max_length=30)
    senha = models.CharField(max_length=30)
    confirmar_senha = models.CharField(max_length=30)

    def __str__(self):
        return self.nome


class Cadastro(models.Model):
    nome = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    senha = models.EmailField(max_length=30)
    confirmar_senha = models.CharField(max_length=30)

    def __str__(self):
        return self.nome

        

