from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login


#funcao de login do usuario
def login(request):
    if request.method == 'GET':
        return render(request, 'login.html')
    if request.method == 'POST':
        nome = request.POST.get('nome')
        senha = request.POST.get('senha')
        confirmar_senha = request.POST.get('confirmar_senha')

        if not senha:
            messages.error(request, ('Insira uma senha.'))
            return render(request, 'login.html')

        if len(senha) < 8:
            messages.error(request, 'A senha deve ter no minimo 8 caracteres.')
            return render(request, 'login.html')

        if senha != confirmar_senha:
            messages.error(request, 'As senhas devem ser iguais')
            return render(request, 'login.html')

        user = authenticate(username=nome, password=senha)

        if not user:
            messages.error(request, 'Nome ou senha invalidos.')
            return render(request, 'login.html')
        
        login(request, user)
        messages.success(request, 'Login realizado com sucesso.')
        

